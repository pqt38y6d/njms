const express = require('express')
const exphbs = require('express-handlebars')
const fileRoutes = require('./routes/route')


const PORT = process.env.PORT || 3000

const app = express()

const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs'
})

app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')
app.set('views', 'views')

app.use(fileRoutes)


async function start() {
    try {
        app.listen(PORT, () => {
            console.log("server is running");
        })
    } catch (e) {
        console.log(e);
    }
}

start();


